<?php

return [
    'languages.en' => 'English',
    'languages.de' => 'German',
    'menu.settings' => 'Settings',
    'menu.logout' => 'Logout',
    'menu.dashboard' => 'Dashboard',
    'menu.sites' => 'Sites',
    'menu.posts' => 'Posts',
    'menu.languages' => 'Languages',
    'menu.users' => 'Users',
    'menu.helpButton.title' => 'Help',
    'dashboard.gettingStarted' => '',
    'dashboard.gettingStarted.headline' =>'Getting started',
    'dashboard.lastModified' => 'Last modified',
    'dashboard.latestPosts' => 'Latest posts',
    'contenteditor.languageVersion' => 'Language version of site content',
    'contenteditor.addNew' => 'Insert new content element?',
    'contenteditor.deleteContentElement' => 'Delete content element?',
    'contenteditor.deleteButton.title' => 'Delete content element',
    'contenteditor.addButton.title' => 'Add content element',
    'contenteditor.textarea.placeholder' => 'Insert text',
    'posteditor.languageVersion' => 'Language version of post content',
    'posteditor.addNew' => 'Insert new post?',
    'posteditor.deletePost' => 'Delete Post?',
    'posteditor.deleteButton.title' => 'Delete post',
    'posteditor.addButton.title' => 'Add post',
    'languageeditor.setLanguageVersion' => 'Set language versions:',
    'languageeditor.active' => 'Active',
    'languageeditor.default' => 'Default',
    'languageeditor.delete' => 'Delete',
    'languageeditor.addLanguage' => 'Add language',
    'languageeditor.languagePlaceholder' => 'Language',
    'languageeditor.abbreviationPlaceholder' => 'Abbreviation',
    'languageeditor.languageTitle' => 'Specify the name for the new language.',
    'languageeditor.abbreviationTitle' => 'Language abbreviation must consist of two letters.',
    'users.deleteUsers' => 'Delete User?',
    'users.changePassword' => 'Change the password for ',
    'users.user' => 'User',
    'users.role' => 'Role',
    'users.actions' => 'Actions',
    'users.button.deleteUser' => 'Delete user',
    'users.button.changePassword' => 'Change password',
    'users.addUser' => 'Add user',
    'users.language' => 'Language',
    'settings.customLogo' => 'Custom login screen logo',
    'settings.customLogo.fileInfo' => 'Square PNG image file:',
    'settings.customLogo.uploadImage' => 'Upload image',
    'setting.languageSetting' => 'Language setting',
    'settings.changePassword' => 'Change password',
    'button.reset' => 'Reset',
    'button.save' => 'Save',
    'button.yes' => 'Yes',
    'button.no' => 'No',
    'button.delete' => 'Delete',
    'button.register' => 'Register',
    'form.username.placeholder' => 'Username',
    'form.username.title' => 'Minimum three characters.',
    'form.password.placeholder' => 'Password',
    'form.password.title' => 'Your current password.',
    'form.newPassword.placeholder' => 'New Password',
    'form.newPassword.title' => 'Minimum eight characters, at least one number/letter.',
    'form.repeatPassword.placeholder' => 'Repeat Password',
    'form.repeatPassword.title' => 'Repeat your password.',
    'help.dashboard.admin' => '<ul><li>select <i>Languages</i> to manage available language versions and set up the default language of your website</li><li>open the contenteditor by selecting <i>Sites</i> and the filename of the website you want to add content to</li><li>select <i>Posts</i> and the filename you want to add posts to (requires at least one &lt;span&gt; element with the cj-headline, cj-post or cj-date class)</li><li>manage user accounts by selecting <i>Users</i></li><li>Change the back-end language, your password or set up a custom login logo by selecting <i>Settings</i></li></ul>',
    'help.dashboard.chiefEditor' => '<ul><li>select <i>Languages</i> to manage available language versions and set up the default language of your website</li><li>open the contenteditor by selecting <i>Sites</i> and the filename of the website you want to add content to</li><li>select <i>Posts</i> and the filename you want to add posts to (requires at least one &lt;span&gt; element with the cj-headline, cj-post or cj-date class)</li><li>Change the back-end language or your password by selecting <i>Settings</i></li></ul>',
    'help.dashboard.editor' => '<ul><li>select <i>Posts</i> and the filename you want to add posts to (requires at least one &lt;span&gt; element with the cj-headline, cj-post or cj-date class)</li><li>Change the back-end language or your password by selecting <i>Settings</i></li></ul>',
    'help.contenteditor' => '<p>Your website is parsed upon first view of this module.
            Span elements with the cj class are analyzed and their contents
            are stored in a JSON file.</p>

            <p>You can edit these contents via the editor. When visiting your website
            the contents are retrieved from the JSON file with the corresponding
            language (if you stored multiple language versions).</p>

            <p>In case you edited your website (added/deleted &lt;span&gt; elements with the cj class), the JSON file also needs to be changed by adding <img class="helpIcons" src="../resources/img/addButton_274156.svg"> or deleting <img class="helpIcons" src="../resources/img/delete_274156.svg"> contents in the editor.</p>',
    'help.posteditor' => '<p>Your website is parsed upon first view of this module.
                Span elements with the cj-headline, cj-post and cj-date class are analyzed
                and their contents are stored in a JSON file.</p>

            <p>You can edit these contents via the editor. When visiting your website
                the contents of the posts are retrieved from the JSON file with the
                corresponding language (if you stored multiple language versions).</p>

            <p>You can add posts by selecting <img class="helpIcons" src="../resources/img/addButton_274156.svg"> or delete posts by selecting <img class="helpIcons" src="../resources/img/delete_274156.svg">. The number of posts, that are displayed on your website is determined by the number of &lt;span&gt; elements with the cj-headline, cj-post and cj-date class present on your page.</p>',
    'help.languageeditor' => '<p>Select the checkbox &#9745; next to <i>active</i> to activate a language
                version. An activated language version can be selected from the dropdown
                above the contenteditor in Sites and Post to edit the corresponding contents
                of that language.</p>

            <p>If you deactivate a particular language, you can still edit the associated
                contents but they won\'t be displayed on your website. By deleting on of your
                registered languages, the corresponding JSON files are also deleted.</p>

            <p>You can only select one default language. This language version serves as a
            fallback language, that is displayed, if the preferred browser language, which is           determined from the visitors browser, does not match any of your registered
                languages.</p>

            <p>You can add your own language by specifying a name as well as an abbreviation
                and selecting <img class="helpIcons" src="../resources/img/addButton_274156.svg"> and \'Save\' afterwards. Valid abbreviations can be found in the <i>ISO 639-1</i> nomenclature.</p>',
    'help.users' => '<p>Three different roles can be assigned to users:</p>
            <ul>
                <li>Admin: can manage languages, edit sites/posts, manage users
                    and access advanced settings</li>
                <li>Chief Editor: can manage languages and edit sites/posts</li>
                <li>Editor: can edit posts</li>
            </ul>
            <p>You can change the password of a user by selecting <img class="helpIcons" src="../resources/img/changePassword.svg"> and delete a user by selecting <img class="helpIcons" src="../resources/img/delete_274156.svg">.</p>',
    'help.settings.admin' => '<p>Set up a custom login screen logo by uploading your own logo.
                Your image file <i>must</i> be a square PNG and should not be larger than
                2MB.</p>

            <p>You can change the back-end language of the user interface by selecting your
            preferred language from the dropdown. Log out and back in to make this change
                take effect. </p>',
    'help.settings' => '<p>You can change the back-end language of the user interface by selecting your
            preferred language from the dropdown. Log out and back in to make this change
                take effect. </p>',
];
