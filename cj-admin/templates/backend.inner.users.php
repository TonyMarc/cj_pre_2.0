<?php
include_once('../controller/ValidationController.php');
include_once('../controller/RegistrationController.php');

// Include translation config
$translation = include('../config/' . $_SESSION['lang'] . '_lang.php');

if ($_SESSION['type'] < 2) {
    $validation = new ValidationController();
    $registration = new RegistrationController();

    if (isset($_POST['register'])) {
        $type = $_POST['type'];
        $lang = $_POST['lang'];
        if (isset($_POST['username'])) {
            if ($validation->comparePasswords($_POST['password'], $_POST['passwordRepeat'])) {
                if ($registration->createUser($_POST['username'], $validation->createHash($_POST['password']), $type, $lang, '0')) {
                    header('Location: backend.php?view=users');
                    exit;
                }
            }
        }
    }
    if (isset($_POST['delete'])) {
        $username = $_POST['delete'];
        if ($registration->deleteUser($username)) {
            header('Location: backend.php?view=users');
            exit;
        }
    }
    if (isset($_POST['changePassword'])) {
        if ($validation->comparePasswords($_POST['newPassword'], $_POST['newPasswordRepeat'])) {
            $registration->changePassword($_POST['changePassword'], $validation->createHash($_POST['newPassword']));
        }
    }
} else {
    header('Location: backend.php?');
    exit;
}
?>
<h2><?php echo $translation['menu.users']; ?></h2>
<!-- Modals -->
<div id="modalDeleteDialog" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeDeleteDialog">&times;</span>
        <h3><?php echo $translation['users.deleteUsers']; ?></h3>
        <div class="buttonsModal">
            <button type="button"
                    id="deleteNo"><?php echo $translation['button.no']; ?></button>
            <form name="delete" action="" class="form-user-deletion" method="post">
                <button type="submit"
                        name="delete"
                        id="deletion-login"
                        value=""><?php echo $translation['button.delete']; ?></button>
            </form>
        </div>
    </div>
</div>

<div id="modalChangePasswordDialog" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeChangePasswordDialog">&times;</span>
        <h3 id="headlineChangePasswordDialog"><?php echo $translation['users.changePassword']?></h3>
        <div class="buttonsModal">
            <form name="changePassword"
                  action=""
                  class="form-changePassword"
                  method="post"
                  autocomplete="off">
                <?php
                // Output of error messages
                echo $registration->getErrors();
                echo '<br>';
                echo $validation->getErrors();
                ?>
                <div class="formGroup">
                    <input type="password"
                           placeholder="<?php echo $translation['form.newPassword.placeholder']; ?>"
                           name="newPassword"
                           required="required"
                           pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                           title="<?php echo $translation['form.newPassword.title']; ?>"
                           id="inputNewPw"
                           autocomplete="off">
                    <input type="password"
                           placeholder="<?php echo $translation['form.repeatPassword.placeholder']; ?>"
                           name="newPasswordRepeat"
                           required="required"
                           pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                           title="<?php echo $translation['form.repeatPassword.title']; ?>"
                           autocomplete="off"
                           onfocus="comparePasswords(this)">
                    <button name="changePassword"
                            type="submit"
                            id="changePassword"
                            value=""><?php echo $translation['button.save']; ?></button>
                </div>
            </form>

        </div>
    </div>
</div>

<section class="user-area">
    <table class="userTable card" cellspacing="0">
        <tr>
            <th><h3><?php echo $translation['users.user']?></h3></th>
            <th><h3><?php echo $translation['users.role']?></h3></th>
            <th><h3><?php echo $translation['users.actions']?></h3></th>
        </tr>
        <?php
        foreach ($this->_['users'] as $user) {
            ?>
            <tr>
                <td>
                    <?= $user['username'] ?>
                </td>

                <td>
                    <?php
                    if ($user['type'] == 1) {
                        echo 'Admin';
                    }
                    if ($user['type'] == 0) {
                        echo 'Chief Editor';
                    }
                    if ($user['type'] == 2) {
                        echo 'Editor';
                    } ?>
                </td>
                <td>
                    <?php
                    if ($user['username'] != $_SESSION['username']) {
                        echo '<button class="deleteUser" 
                        value="' . $user['username'] . '" onclick="showDeleteModal(this)" title="' . $translation['users.button.deleteUser'] . '">X</button>';
                        echo '<button class="changePassword" value="' . $user['username'] . '" onclick="showChangePasswordModal(this)" title="' . $translation['users.button.changePassword'] . '">C</button>';
                    } ?>
                </td>


            </tr>
            <?php
        } ?>
    </table>

    <div class="card">
        <div class="cardMainUsers">
        <label><h3><?php echo $translation['users.addUser']; ?></h3></label>
        </div>
        <div class="cardFooter">
        <form action="" class="form-registration" method="post" autocomplete="off">
            <?php
            // Output of error messages
            echo $registration->getErrors() . "\n";
            echo $validation->getErrors();
            ?>

            <div class="add-user">
                <div class="formGroupAddUser">
                    <input type="text"
                           placeholder="<?php echo $translation['form.username.placeholder']; ?>"
                           name="username"
                           required="required"
                           pattern="^.{3,}$"
                           title="<?php echo $translation['form.username.title']; ?>"
                           autocomplete="off">
                    <input type="password"
                           id="inputPw"
                           placeholder="<?php echo $translation['form.password.placeholder']; ?>"
                           name="password"
                           required="required"
                           pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                           title="<?php echo $translation['form.newPassword.title']; ?>"
                           autocomplete="off">
                    <input type="password"
                           placeholder="<?php echo $translation['form.repeatPassword.placeholder']; ?>"
                           name="passwordRepeat"
                           required="required"
                           pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                           title="<?php echo $translation['form.repeatPassword.title']; ?>"
                           onfocus="compareNewUserPasswords(this)"
                           autocomplete="off"
                           id="inputPwRepeat">


                    <div class="is-admin">
                        <select name="type"
                                required="required"
                                id="user-role"
                                title="Select user role.">
                            <option disabled selected value><?php echo $translation['users.role']?></option>
                            <option value="1">Admin</option>
                            <option value="0">Chief Editor</option>
                            <option value="2">Editor</option>
                        </select>
                    </div>

                    <div class="backendLanguage">
                        <select name="lang"
                                required="required"
                                id="backendLanguage"
                                title="Select preferred language.">
                            <option disabled selected value><?php echo $translation['users.language']?></option>
                            <?php
                            $config = include ('../config/config.php');
                            foreach ($config['translations'] as $lang){
                                echo '<option value="'. $lang .'"';
                                $translationKey = 'languages.' . $lang;
                                echo '>' . $translation[$translationKey] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="register">
                        <button type="submit"
                                name="register"
                                id="registration-login"
                                value="update"><?php echo $translation['button.register']; ?></button>
                    </div>
                </div>
        </form>
        </div>
    </div>
</section>

<script src="../resources/js/users.js"></script>
<script src="../resources/js/pattern.js"></script>

