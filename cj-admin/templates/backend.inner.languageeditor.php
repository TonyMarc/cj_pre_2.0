<?php
// Include translation config
$translation = include ('../config/'. $_SESSION['lang'] .'_lang.php');

if ($_SESSION['type'] < 2) {
?>

<h2><?php echo $translation['languageeditor.setLanguageVersion'] ?></h2>

<form class="languages-form" method="post">
    <div class="languages-wrapper">
        <?php
        foreach ($this->_['languages'] as $language) {
            ?>
            <fieldset>
                <h3><?= $language['long'] ?> (<?= $language['short'] ?>)</h3>
                <input type="hidden" value="<?= $language['long'] ?>" name="long[]">
                <!--TODO restrict length-->
                <input type="hidden" value="<?= $language['short'] ?>" name="short[]">
                <label><?php echo $translation['languageeditor.active'] ?></label>
                <input id="languageActive" class="active" type="checkbox" value="<?= $language['short'] ?>"
                       name="isActive[]" <?php if ($language['isActive'] == 1) {
                echo "checked";
            } ?>>
                <label><?php echo $translation['languageeditor.default'] ?></label>
                <input id="languageDefault" class="default" type="radio" value="<?= $language['short'] ?>"
                       name="isDefault" <?php if ($language['isDefault'] == 1) {
                echo "checked";
            } ?> onclick="defaultSetActiveAndDelete(this)">

                <label><?php echo $translation['languageeditor.delete'] ?></label>
                <input id="languageDelete" class="delete" type="checkbox" value="<?= $language['short'] ?>" name="toDelete[]">

            </fieldset>
        <?php
        } ?>
    </div>
    <div class="save">
        <button type="submit"><?php echo $translation['button.save'] ?></button>
    </div>
</form>

<form class="add-language-form">
    <fieldset>
        <h3><?php echo $translation['languageeditor.addLanguage'] ?></h3>
    <input type="text"
           placeholder="<?php echo $translation['languageeditor.languagePlaceholder'] ?>"
           name="add-long"
           title="<?php echo $translation['languageeditor.languageTitle'] ?>">
    <input type="text"
           placeholder="<?php echo $translation['languageeditor.abbreviationPlaceholder'] ?>"
           name="add-short"
           maxlength="2"
           title="<?php echo $translation['languageeditor.abbreviationTitle'] ?>">
    <input type="button"
           id="add-language"
           value="+"
           onclick="appendLanguageFieldset(this)"
           title="<?php echo $translation['languageeditor.addLanguage']?>">
    </fieldset>
</form>
<?php
} else {
    header('Location: backend.php?');
    exit;
}
?>
<script src="../resources/js/languageeditor.js"></script>
