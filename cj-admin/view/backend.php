<?php
session_start();
include_once('../controller/BackendController.php');
include_once('../model/View.php');
// is true if a user is logged in


$isUserLoggedIn = isset($_SESSION['username']);


// the user is correctly logged in
if ($isUserLoggedIn) {
    $request = array_merge($_GET, $_POST);
    $controller = new BackendController($request);
    $controller->handleGetOrPost();

// redirect to login if user is not logged in
} else {
    header('Location: login.php');
    exit;
}
