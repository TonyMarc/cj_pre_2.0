/**
 * Toggles menu in responsive mode (min-width: 700px)
 */
function menu() {
    var aside = document.getElementById("asideNav");
    var nav = document.getElementById("navNav");
    var main = document.getElementById("main");
    var menuButton = document.getElementById("menuButton");

    // Get user menu
    var userMenu = document.getElementById("userMenu");
    var overlay = document.getElementById("overlay");

    if (aside.className === "aside") {
        aside.className += " responsive";
        nav.className += " responsive";
        main.className += " responsive";
        menuButton.className += " responsive";
        // Check if user menu is open
        if (userMenu.className !== "userMenu") {
            overlay.style.display = "none";
            userMenu.className = "userMenu";
        }
    } else {
        aside.className = "aside";
        nav.className = "nav";
        main.className = "content";
        menuButton.className = "icon";
    }
}

/**
 * Toggles user-menu
 */
function userMenu() {
    var userMenu = document.getElementById("userMenu");
    var overlay = document.getElementById("overlay");

    // Get the main menu
    var aside = document.getElementById("asideNav");
    var nav = document.getElementById("navNav");
    var main = document.getElementById("main");
    var menuButton = document.getElementById("menuButton");


    if (userMenu.className === "userMenu") {
        overlay.style.display = "block";
        userMenu.className += " responsive";
        // Check if main menu is open
        if (aside.className !== "aside") {
            aside.className = "aside";
            nav.className = "nav";
            main.className = "content";
            menuButton.className = "icon";
        }
    } else {
        overlay.style.display = "none";
        userMenu.className = "userMenu";
    }
    // Closes overlay if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == overlay) {
            overlay.style.display = "none";
            userMenu.className = "userMenu";
        }
    };
}
