<?php

/**
 * Class RegistrationController
 *
 * This class handles addition and deletion of users to .shadow file
 */
class RegistrationController
{
    /**
     * @var string
     */
    public $errorMessage = '';

    /**
     * This method checks the given username and password and
     * saves the user if the given input is valid
     *
     * @param string $username
     * @param string $passwordHash
     * @param string $userType
     * @param string $backendLanguage
     * @param $tagSetting
     * @return bool true if the user creation was successful
     */
    public function createUser($username, $passwordHash, $userType, $backendLanguage, $tagSetting)
    {
        if (strlen($username) >= 3) {
            if (!empty($passwordHash)) {
                if (file_exists("../etc/.shadow")) {
                    if ($this->isUsernameAlreadyTaken($username)) {
                        $this->errorMessage = 'Username already taken';
                        return false;
                    } else {
                        $newUser = $this->beautifyString($username, $passwordHash, $userType, $backendLanguage, $tagSetting);
                        file_put_contents("../etc/.shadow", $newUser . PHP_EOL, FILE_APPEND | LOCK_EX);
                        return true;
                    }
                } else {
                    $newUser = $this->beautifyString($username, $passwordHash, $userType, $backendLanguage, $tagSetting);
                    file_put_contents("../etc/.shadow", $newUser . PHP_EOL, FILE_APPEND | LOCK_EX);
                    return true;
                }
            } else {
                $this->errorMessage = "Password must be at least 8 characters.";
                return false;
            }
        } else {
            $this->errorMessage = 'Username must be at least 3 characters.';
            return false;
        }
    }

    /**
     * This method handles the deletion of users.
     * The last admin user cannot be deleted and a user cannot delete herself
     *
     * @param string $username the user to be deleted
     * @return bool returns true if the deletion was successful
     */
    public function deleteUser($username)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);

            $tmp = array_filter($shadows);

            if (count($tmp) == 1 || $_SESSION['username'] === $username) {
                $this->errorMessage = 'At least one admin user must be present.';
                return false;
            } else {
                $key = $this->substringInArray($username, $shadows);

                unset($shadows[$key]);
                file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
                echo '<span class="infoMessage">User ' . $username . ' was deleted</span>';
                return true;
            }
        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Changes the password of a user
     *
     * @param string $username
     * @param string $passwordHash
     * @return bool returns true if password was changed
     */
    public function changePassword($username, $passwordHash)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);
            $key = $this->substringInArray($username, $shadows);
            $tmp = list($username, $password, $type, $lang, $tag) = explode(":", $shadows[$key]);
            $newLine = $this->beautifyString($tmp[0], $passwordHash, $tmp[2], $tmp[3], $tmp[4]);

            $shadows[$key] = $newLine;
            file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
            echo '<span class="infoMessage">Your password was changed.</span>';
            return true;

        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Changes the preferred back-end language of a user
     *
     * @param string $username
     * @param string $backendLanguage
     * @return bool returns true if language was changed
     */
    public function changeBackendLanguage($username, $backendLanguage)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);
            $key = $this->substringInArray($username, $shadows);
            $tmp = list($username, $password, $type, $lang, $tag) = explode(":", $shadows[$key]);
            $newLine = $this->beautifyString($tmp[0], $tmp[1], $tmp[2], $backendLanguage, $tmp[4]);
            $shadows[$key] = $newLine;
            file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
            return true;

        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Change tag (user setting)
     *
     * @param string $username
     * @param $tagSetting
     * @return bool returns true if tag was changed
     */
    public function changeTag($username, $tagSetting)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);
            $key = $this->substringInArray($username, $shadows);
            $tmp = list($username, $password, $type, $lang, $tag) = explode(":", $shadows[$key]);
            $newLine = $this->beautifyString($tmp[0], $tmp[1], $tmp[2], $tmp[3], $tagSetting);

            $shadows[$key] = $newLine;
            file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
            return true;

        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Checks for all strings in the values of an associative
     * array and returns the according key
     *
     * @param string $string the string to search for
     * @param array $array the associative array to look through
     * @return bool|int|string returns the key to the value
     * or false if the value could not be found.
     */
    public function substringInArray($string, array $array)
    {
        foreach ($array as $key => $value) {
            if (strpos($value, $string) !== false) {
                return $key;
            }
        }
        return false;
    }

    /**
     * Formats the username, password hash, user type and backend
     * language according to .shadow file layout
     *
     * @param string $username the given username
     * @param string $passwordHash the given password hash
     * @param string $userType the given user type
     * @param string $backendLanguage
     * @param $tagSetting
     * @return string
     */
    private function beautifyString($username, $passwordHash, $userType, $backendLanguage, $tagSetting)
    {
        $beautifiedString = $username . ':' . $passwordHash . ':' . $userType . ':' . $backendLanguage . ':' . $tagSetting;
        return $beautifiedString;
    }

    /**
     * Checks if the username is already present in .shadow
     *
     * @param string $username
     * @return bool true if the username already exists
     */
    private function isUsernameAlreadyTaken($username)
    {
        // TODO replace with getUsers() from FileHandler class
        $fn = fopen("../etc/.shadow", "r") or die("fail to open file");
        $credentials = array();
        while ($row = fgets($fn)) {
            list($name) = explode(":", $row);

            $credentials[] = array(
                'username' => $name,
            );
        }
        fclose($fn);

        $shadows = $credentials;
        foreach ($shadows as $key => $value) {
            if ($value['username'] === $username) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns error messages
     *
     * @return string
     */
    public function getErrors()
    {
        return $this->errorMessage;
    }
}
