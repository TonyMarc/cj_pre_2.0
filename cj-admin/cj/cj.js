/**
 * This javascript file handles the exchange the cj span's
 * content from the corresponding content JSON file
 */

var file = '';
var lang = '';
var languages;

/**
 * On page load
 */
fetchJSONFile('cj-content/languages/language.json', function (json) {
    exchangeContent(json);
    //loadPhp('cj-admin/cj/analytics.php');
});

function exchangeContent(languages)
{
    this.languages = languages;
    // Sets available languages and default language
    var cjLanguages = [];
    var defaultLang = '';
    for (var i = 0; i < languages.length; i++) {
        if (languages[i].isActive) {
            cjLanguages.push(languages[i].short);
        }
        if (languages[i].isDefault) {
            defaultLang = languages[i].short;
        }
    }

    var localStorage = null;

    // determine html file name, index represents default value
    var url = window.location.pathname;
    if (window.location.href.match(/(html|htm|php|php3)/)) {
        var filename = url.substring(url.lastIndexOf('/') + 1);
        file = filename.substring(0, filename.indexOf('.'));
    } else {
        file = "index";
    }

    // detect the preferred browser language
    try {
        if (window.sessionStorage.getItem('lang')) {
            lang = window.sessionStorage.getItem('lang');
        } else {
            if (typeof navigator.language && navigator.languages[0] !== 'undefined') {
                var navLanguage = navigator.language;
                var navLanguages = navigator.languages[0];
                if (navLanguage.substr(0, 2) == navLanguages.substr(0, 2)) {
                    lang = navLanguage.substr(0, 2);
                } else {
                    lang = navLanguages.substr(0, 2);
                }
            } else {
                var userLang = navigator.language || navigator.languages[0];
                lang = userLang.substr(0, 2);
            }
        }
        localStorage = true;
    } catch (err) {
        localStorage = false;
    }

    try {
        if (!localStorage) {
            lang = window.name;
        }
    } catch (err) {
        //TODO handle error
    }

    /*
     * JSON file is loaded, determined by preferred browser language and
     * html file name
     * If no {file}_{lang}.json file is found, the predetermined default
     * {file}_{defaultLang}.json is loaded
     */
    if (cjLanguages.indexOf(lang) > -1) {
        var fullFilename = 'cj-content/sites/' + file + '_' + lang + '.json';
        /*
         * Requests file and executes callback with parsed result once
         * it is available
         */
        fetchJSONFile(fullFilename, function (json) {
            var cjClass = document.querySelectorAll('[class="cj"]');
            for (var i = 0; i < cjClass.length; i++) {
                if (json[i].content != '' && cjClass[i].innerHTML == ''){
                    cjClass[i].innerHTML = json[i].content;
                }
                if (json[i].content != '' && cjClass[i].innerHTML != '') {
                    cjClass[i].innerHTML = json[i].content;
                }
            }
        });
    } else {
        var fullFilenameDefaultLang = 'cj-content/sites/' + file + '_' + defaultLang + '.json';
        /*
         * Requests file and executes callback with parsed result once
         * it is available
         */
        fetchJSONFile(fullFilenameDefaultLang, function (json) {
            var cjClass = document.querySelectorAll('[class="cj"]');
            for (var i = 0; i < cjClass.length; i++) {
                if (json[i].content != '' && cjClass[i].innerHTML == ''){
                    cjClass[i].innerHTML = json[i].content;
                }
                if (json[i].content != '' && cjClass[i].innerHTML != '') {
                    cjClass[i].innerHTML = json[i].content;
                }
            }
        });
    }

    /*
     * Cj posts
     */
    try {
        var cjHeadline = document.querySelectorAll('[class="cj-headline"]');
        var cjPost = document.querySelectorAll('[class="cj-post"]');
        var cjDate = document.querySelectorAll('[class="cj-date"]');

        if (cjHeadline.length > 0) {
            /*
             * JSON file is loaded, determined by preferred browser language and
             * html file name
             * If no {file}_{lang}.json file is found, the predetermined default
             * {file}_{defaultLang}.json is loaded
             */
            if (cjLanguages.indexOf(lang) > -1) {
                var fullFilename = 'cj-content/posts/' + file + '_' + lang + '.json';
                /*
                 * Requests file and executes callback with parsed result once
                 * it is available
                 */
                fetchJSONFile(fullFilename, function (json) {
                    for (var i = 0; i < cjHeadline.length; i++) {
                        if (json[i].headline != '' && cjHeadline[i].innerHTML == ''){
                            cjHeadline[i].innerHTML = json[i].headline;
                        }
                        if (json[i].headline != '' && cjHeadline[i].innerHTML != '') {
                            cjHeadline[i].innerHTML = json[i].headline;
                        }
                    }
                });
            } else {
                var fullFilenameDefaultLang = 'cj-content/posts/' + file + '_' + defaultLang + '.json';
                /*
                 * Requests file and executes callback with parsed result once
                 * it is available
                 */
                fetchJSONFile(fullFilenameDefaultLang, function (json) {
                    for (var i = 0; i < cjHeadline.length; i++) {
                        if (json[i].headline != '' && cjHeadline[i].innerHTML == ''){
                            cjHeadline[i].innerHTML = json[i].headline;
                        }
                        if (json[i].headline != '' && cjHeadline[i].innerHTML != '') {
                            cjHeadline[i].innerHTML = json[i].headline;
                        }
                    }
                });
            }
        }

        if (cjPost.length > 0) {
            /*
             * JSON file is loaded, determined by preferred browser language and
             * html file name
             * If no {file}_{lang}.json file is found, the predetermined default
             * {file}_{defaultLang}.json is loaded
             */
            if (cjLanguages.indexOf(lang) > -1) {
                var fullFilename = 'cj-content/posts/' + file + '_' + lang + '.json';
                /*
                 * Requests file and executes callback with parsed result once
                 * it is available
                 */
                fetchJSONFile(fullFilename, function (json) {
                    for (var i = 0; i < cjPost.length; i++) {
                        if (json[i].post != '' && cjPost[i].innerHTML == ''){
                            cjPost[i].innerHTML = json[i].post;
                        }
                        if (json[i].post != '' && cjPost[i].innerHTML != '') {
                            cjPost[i].innerHTML = json[i].post;
                        }
                    }
                });
            } else {
                var fullFilenameDefaultLang = 'cj-content/posts/' + file + '_' + defaultLang + '.json';
                /*
                 * Requests file and executes callback with parsed result once
                 * it is available
                 */
                fetchJSONFile(fullFilenameDefaultLang, function (json) {
                    for (var i = 0; i < cjPost.length; i++) {
                        if (json[i].post != '' && cjPost[i].innerHTML == ''){
                            cjPost[i].innerHTML = json[i].post;
                        }
                        if (json[i].post != '' && cjPost[i].innerHTML != '') {
                            cjPost[i].innerHTML = json[i].post;
                        }
                    }
                });
            }
        }

        if (cjDate.length > 0) {
            /*
             * JSON file is loaded, determined by preferred browser language and
             * html file name
             * If no {file}_{lang}.json file is found, the predetermined default
             * {file}_{defaultLang}.json is loaded
             */
            if (cjLanguages.indexOf(lang) > -1) {
                var fullFilename = 'cj-content/posts/' + file + '_' + lang + '.json';
                /*
                 * Requests file and executes callback with parsed result once
                 * it is available
                 */
                fetchJSONFile(fullFilename, function (json) {
                    for (var i = 0; i < cjDate.length; i++) {
                        if (json[i].date != '' && cjDate[i].innerHTML == ''){
                            cjDate[i].innerHTML = json[i].date;
                        }
                        if (json[i].date != '' && cjDate[i].innerHTML != '') {
                            cjDate[i].innerHTML = json[i].date;
                        }
                    }
                });
            } else {
                var fullFilenameDefaultLang = 'cj-content/posts/' + file + '_' + defaultLang + '.json';
                /*
                 * Requests file and executes callback with parsed result once
                 * it is available
                 */
                fetchJSONFile(fullFilenameDefaultLang, function (json) {
                    for (var i = 0; i < cjDate.length; i++) {
                        if (json[i].date != '' && cjDate[i].innerHTML == ''){
                            cjDate[i].innerHTML = json[i].date;
                        }
                        if (json[i].date != '' && cjDate[i].innerHTML != '') {
                            cjDate[i].innerHTML = json[i].date;
                        }
                    }
                });
            }
        }
    } catch (err) {

    }
}

/**
 * This method fetches the contents of a given json file,
 * pareses it and executes the callback on the content
 *
 * @param path the path to the json file
 * @param callback the function to be executed on the fetched json
 */
function fetchJSONFile(path, callback)
{
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200 || httpRequest.status === 0) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) {
                    callback(data);
                }
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}

/**
 * This method can be used to access the value of an input
 * field and thus exchange the content of the page
 *
 * @param parameter that is transmitted via the input field
 */
function cjLangSelect(parameter)
{
    var browserStorage = null;
    var langSelected;

    if (parameter.length > 2) {
        langSelected = parameter.value;
    } else {
        langSelected = parameter;
    }

    try {
        window.sessionStorage.setItem('lang', langSelected);
        browserStorage = true;
    } catch (err) {
        browserStorage = false;
    }
    if (!browserStorage) {
        window.name = langSelected;
    }

    exchangeContent(this.languages);
}

/**
 * @param path to the php file
 */
function loadPhp(path)
{
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200 || httpRequest.status === 0) {
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}
